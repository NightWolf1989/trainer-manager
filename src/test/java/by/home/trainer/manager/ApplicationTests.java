package by.home.trainer.manager;

import by.home.trainer.manager.dto.admin.AdminCreateDto;
import by.home.trainer.manager.dto.admin.AdminFullDto;
import by.home.trainer.manager.dto.admin.AdminPreviewDto;
import by.home.trainer.manager.dto.admin.AdminUpdateDto;
import by.home.trainer.manager.entity.Admin;
import by.home.trainer.manager.entity.Comment;
import by.home.trainer.manager.entity.Trainer;
import by.home.trainer.manager.entity.enums.Role;
import by.home.trainer.manager.repository.*;
import by.home.trainer.manager.service.AdminService;
import by.home.trainer.manager.service.AdminServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ApplicationTests {

	private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
	private AdminRepository adminRepository = new AdminRepositoryImpl();
	private CommentRepository commentRepository = new CommentRepositoryImpl();
	private AdminService adminService = new AdminServiceImpl();

	@BeforeEach
	void setUp(){
		adminRepository.deleteAll();
		commentRepository.deleteAll();
		trainerRepository.deleteAll();
	}

	@Test
	void foundByIdDto(){
		//given
		Admin admin = Admin.builder()
				.name("Bob")
				.surname("Bobson")
				.email("BBobson@mail.ru")
				.password("12345677")
				.role(Role.SUPERUSER)
				.build();

		adminRepository.create(admin);

		//when
		AdminFullDto foundAdmin = adminService.findById(admin.getId());

		//then
		Assertions.assertNotNull(foundAdmin);
		Assertions.assertEquals(admin.getId(), foundAdmin.getId());

	}

	@Test
	void findAllAdmin(){
		//given
		Admin admin1 = Admin.builder()
				.name("Bob1")
				.surname("Bobson1")
				.email("BBobson@mail.ru1")
				.password("123456771")
				.role(Role.SUPERUSER)
				.build();

		Admin admin2 = Admin.builder()
				.name("Bob2")
				.surname("Bobson2")
				.email("BBobson@mail.ru2")
				.password("123456772")
				.role(Role.SUPERUSER)
				.build();

		Admin admin3 = Admin.builder()
				.name("Bob3")
				.surname("Bobson3")
				.email("BBobson@mail.ru3")
				.password("123456773")
				.role(Role.SUPERUSER)
				.build();

		adminRepository.create(admin1);
		adminRepository.create(admin2);
		adminRepository.create(admin3);


		//when
		List<AdminPreviewDto> listAdmin = adminService.findAll();

		//then
		Assertions.assertEquals(3,listAdmin.size());

	}

	@Test
	void testCreateAdmin(){
		//given
		AdminCreateDto createDto = AdminCreateDto.builder()
				.name("Bob")
				.surname("Bobson")
				.password("12345677")
				.email("BBobson@gmail.ru")
				.build();

		//TODO Роль Admin даю в AdminServiceImpl

		//when
		AdminFullDto save = adminService.create(createDto);

		//then
		Assertions.assertNotNull(save);

	}

	@Test
	void testUpdateAdminDto() {
		//given
		AdminCreateDto admin = AdminCreateDto.builder()
				.name("Bob")
				.surname("Bobson")
				.email("BBobson@mail.ru")
				.password("12345677")
				.build();

		AdminFullDto save = adminService.create(admin);

		AdminUpdateDto updateDto = AdminUpdateDto.builder()
				.id(save.getId())
				.name("DtoBob")
				.surname("DtoBobson")
				.build();

		//when
		AdminFullDto fullDto = adminService.update(updateDto);

		//then
		Assertions.assertNotNull(fullDto.getName());


	}

	@Test
	void testCreate() {
		//given
		Admin admin = Admin.builder()
				.name("Bob")
				.surname("Bobson")
				.email("BBobson@mail.ru")
				.password("12345677")
				.role(Role.SUPERUSER)
				.build();
		//when
		Admin saved = adminRepository.create(admin);

		//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void testFindAll() {
		//given
		Admin admin1 = Admin.builder()
				.name("Bob1")
				.surname("Bobson1")
				.email("BBobson1@mail.ru")
				.password("123456771")
				.role(Role.SUPERUSER)
				.build();

		Admin admin2 = Admin.builder()
				.name("Bob2")
				.surname("Bobson2")
				.email("BBobson2@mail.ru")
				.password("123456772")
				.role(Role.ADMIN)
				.build();

		adminRepository.create(admin1);
		adminRepository.create(admin2);

		//when
		List<Admin> found = adminRepository.findAll();

		//then
		Assertions.assertEquals(2, found.size());
	}

	@Test
	void testUpdate() {
		//given
		Admin admin1 = Admin.builder()
				.name("Bob1")
				.surname("Bobson1")
				.email("BBobson1@mail.ru")
				.password("123456771")
				.role(Role.SUPERUSER)
				.build();

		adminRepository.create(admin1);

		//when

		Long id = admin1.getId();

		Admin admin2 = Admin.builder()
				.id(admin1.getId())
				.name("Bob2")
				.surname("Bobson2")
				.email("BBobson2@mail.ru")
				.password("123456772")
				.role(Role.ADMIN)
				.build();

		adminRepository.update(admin2);


		//then
		Assertions.assertNotNull(admin2);
		Assertions.assertEquals(id,admin2.getId());

	}

	@Test
	void testDelete() {
		//given
		Admin admin1 = Admin.builder()
				.name("Bob1")
				.surname("Bobson1")
				.email("BBobson1@mail.ru")
				.password("123456771")
				.role(Role.SUPERUSER)
				.build();

		Admin admin2 = Admin.builder()
				.name("Bob2")
				.surname("Bobson2")
				.email("BBobson2@mail.ru")
				.password("123456772")
				.role(Role.ADMIN)
				.build();

		adminRepository.create(admin1);
		adminRepository.create(admin2);

		//when
		adminRepository.deleteById(admin1.getId());
		List<Admin> found = adminRepository.findAll();

		//then
		Assertions.assertEquals(1, found.size());
	}

	@Test
	void testFoundById() {
		//given
		Admin admin = Admin.builder()
				.name("Bob")
				.surname("Bobson")
				.email("BBobson@mail.ru")
				.password("12345677")
				.role(Role.SUPERUSER)
				.build();

		adminRepository.create(admin);

		//when
		Admin found = adminRepository.findById(admin.getId());

		//then
		Assertions.assertEquals(admin, found);
	}

	@Test
	void save_trainerWithoutComment(){
		//given
		Trainer trainer = Trainer.builder()
				.name("Bob")
				.surname("Bobson")
				.imageAvatar("image")
				.email("BBobson@mail.ru")
				.password("12345677")
				.build();

		//when
		Trainer saved = trainerRepository.create(trainer);

		//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void save_trainerWithComment() {
		//given
		Trainer trainer = Trainer.builder()
				.name("Bob")
				.surname("Bobson")
				.imageAvatar("image")
				.email("BBobson@mail.ru")
				.password("12345677")
				.build();

		Trainer saved = trainerRepository.create(trainer);

		Comment comment1 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex")
				.userSurname("Coins")
				.userEmail("CAlex@mail.ru")
				.trainer(trainer)
				.build();

		Comment comment2 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex2")
				.userSurname("Coins2")
				.userEmail("CAlex2@mail.ru")
				.trainer(trainer)
				.build();

		commentRepository.create(comment1);
		commentRepository.create(comment2);


		//then
		Assertions.assertNotNull(saved.getId());

	}

	@Test
	void findById_happyPath(){
		//given
		Trainer trainer = Trainer.builder()
				.name("Bob")
				.surname("Bobson")
				.imageAvatar("image")
				.email("BBobson@mail.ru")
				.password("12345677")
				.build();

		Trainer saved = trainerRepository.create(trainer);

		Comment comment1 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex")
				.userSurname("Coins")
				.userEmail("CAlex@mail.ru")
				.trainer(trainer)
				.build();

		Comment comment2 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex2")
				.userSurname("Coins2")
				.userEmail("CAlex2@mail.ru")
				.trainer(trainer)
				.build();

		commentRepository.create(comment1);
		commentRepository.create(comment2);
		//when
		Trainer found = trainerRepository.findById(saved.getId());

		//then
		Assertions.assertNotNull(found);
		Assertions.assertNotNull(saved.getId());
		Assertions.assertNotNull(found.getComments());
		Assertions.assertEquals(2, found.getComments().size());
	}

	@Test
	void findAllWhiteoutsComments(){
		//given
		Trainer trainer = Trainer.builder()
				.name("Bob")
				.surname("Bobson")
				.imageAvatar("image")
				.email("BBobson@mail.ru")
				.password("12345677")
				.build();

		Trainer trainer2 = Trainer.builder()
				.name("Bob2")
				.surname("Bobson2")
				.imageAvatar("image2")
				.email("BBobson2@mail.ru")
				.password("123456772")
				.build();

		Trainer saved = trainerRepository.create(trainer);
		Trainer saved2 = trainerRepository.create(trainer2);

		Comment comment1 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex")
				.userSurname("Coins")
				.userEmail("CAlex@mail.ru")
				.trainer(trainer)
				.build();

		Comment comment2 = Comment.builder()
				.message("Hello Bob")
				.userName("Alex2")
				.userSurname("Coins2")
				.userEmail("CAlex2@mail.ru")
				.trainer(trainer)
				.build();

		commentRepository.create(comment1);
		commentRepository.create(comment2);
		//when
		List<Trainer> foundAll = trainerRepository.findAll();

		//then
		Assertions.assertNotNull(foundAll);
		Assertions.assertNull(trainer.getComments());
		Assertions.assertEquals(2, foundAll.size());

	}
}
