package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Recording;

import java.util.List;

public interface RecordingRepository {

    List<Recording> findAll();

    Recording findById(Long id);

    Recording create(Recording recording);

    Recording update(Recording recording);

    void deleteById(Long id);

    void deleteAll();
}
