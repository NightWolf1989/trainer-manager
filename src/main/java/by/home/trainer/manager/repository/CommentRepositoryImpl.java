package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Comment;
import by.home.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM comment", Comment.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundById = em.find(Comment.class, id);

        em.close();
        return foundById;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();

        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();

        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment fondComment = em.find(Comment.class, id);
        em.remove(fondComment);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
            EntityManager em = EntityManagerUtils.getEntityManager();
            em.getTransaction().begin();

            em.createNativeQuery("DELETE FROM comment").executeUpdate();

            em.getTransaction().commit();
            em.close();
    }
}

