package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> findAll();

    Comment findById(Long id);

    Comment create(Comment comment);

    Comment update(Comment comment);

    void deleteById(Long id);

    void deleteAll();
}
