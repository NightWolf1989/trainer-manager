package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Trainer;
import by.home.trainer.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public List<Trainer> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Trainer> foundList = em.createNativeQuery("SELECT * FROM trainer", Trainer.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Trainer findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Trainer foundById = em.find(Trainer.class, id);

        Hibernate.initialize(foundById.getComments());
        Hibernate.initialize(foundById.getRecordings());

        em.close();

        return foundById;
    }

    @Override
    public Trainer create(Trainer trainer) {
        EntityManager em = null;
        try {
            em = EntityManagerUtils.getEntityManager();
            em.getTransaction().begin();

            em.persist(trainer);

            em.getTransaction().commit();
        }catch (Exception e){
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return trainer;
    }

    @Override
    public Trainer update(Trainer trainer) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(trainer);

        em.getTransaction().commit();
        em.close();

        return trainer;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Trainer foundTrainer = em.find(Trainer.class, id);
        em.remove(foundTrainer);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
