package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Admin;

import java.util.List;

public interface AdminRepository {

    List<Admin> findAll();

    Admin findById(Long id);

    Admin create (Admin admin);

    Admin update(Admin admin);

    void deleteById(Long id);

    void deleteAll();
}
