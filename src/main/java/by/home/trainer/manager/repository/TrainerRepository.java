package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Trainer;

import java.util.List;

public interface TrainerRepository {

    List<Trainer> findAll();

    Trainer findById(Long id);

    Trainer create(Trainer trainer);

    Trainer update(Trainer trainer);

    void deleteById(Long id);

    void deleteAll();
}
