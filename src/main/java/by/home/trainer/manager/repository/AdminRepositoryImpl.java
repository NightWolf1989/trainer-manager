package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Admin;
import by.home.trainer.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminRepositoryImpl implements AdminRepository {
    @Override
    public List<Admin> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Admin> foundList = em.createNativeQuery("SELECT * FROM admin ", Admin.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Admin findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Admin foundAdmin = em.find(Admin.class, id);

        em.close();

        return foundAdmin;
    }

    @Override
    public Admin create(Admin admin) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(admin);

        em.getTransaction().commit();
        em.close();

        return admin;
    }

    @Override
    public Admin update(Admin admin) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(admin);

        em.getTransaction().commit();
        em.close();

        return admin;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Admin foundAdmin = em.find(Admin.class, id);
        em.remove(foundAdmin);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM admin").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
