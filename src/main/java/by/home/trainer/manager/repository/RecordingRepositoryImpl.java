package by.home.trainer.manager.repository;

import by.home.trainer.manager.entity.Recording;
import by.home.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class RecordingRepositoryImpl implements RecordingRepository {
    @Override
    public List<Recording> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Recording> foundList = em.createNativeQuery("SELECT * FROM recording", Recording.class)
                .getResultList();
        return foundList;
    }

    @Override
    public Recording findById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Recording foundById = em.find(Recording.class, id);

        em.close();
        return foundById;
    }

    @Override
    public Recording create(Recording recording) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(recording);

        em.getTransaction().commit();
        em.close();

        return recording;
    }

    @Override
    public Recording update(Recording recording) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(recording);

        em.getTransaction().commit();
        em.close();

        return recording;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Recording foundRecording = em.find(Recording.class, id);
        em.remove(foundRecording);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM recording").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
