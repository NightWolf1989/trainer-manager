package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.admin.AdminCreateDto;
import by.home.trainer.manager.dto.admin.AdminFullDto;
import by.home.trainer.manager.dto.admin.AdminPreviewDto;
import by.home.trainer.manager.dto.admin.AdminUpdateDto;
import by.home.trainer.manager.entity.Admin;
import by.home.trainer.manager.entity.enums.Role;
import by.home.trainer.manager.mapper.AdminMapper;
import by.home.trainer.manager.repository.AdminRepositoryImpl;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    private AdminRepositoryImpl adminRepository = new AdminRepositoryImpl();
    private AdminMapper mapper = new AdminMapper();

    @Override
    public List<AdminPreviewDto> findAll() {
        List<Admin> found = adminRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public AdminFullDto findById(Long id) {
        Admin foundById = adminRepository.findById(id);

        AdminFullDto response = mapper.mapToDto(foundById);
        return response;
    }

    @Override
    public AdminFullDto create(AdminCreateDto createDto) {
        Admin toSave = mapper.mapToEntity(createDto);

        toSave.setRole(Role.ADMIN);
        Admin create = adminRepository.create(toSave);

        AdminFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public AdminFullDto update(AdminUpdateDto updateDto) {
        Admin entityToUpdate = mapper.mapToEntity(updateDto);
        Admin existingEntity = adminRepository.findById(updateDto.getId());

       entityToUpdate.setRole(existingEntity.getRole());
       entityToUpdate.setPassword(existingEntity.getPassword());
       entityToUpdate.setEmail(existingEntity.getEmail());

       Admin create = adminRepository.update(entityToUpdate);

       AdminFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
        adminRepository.deleteById(id);
    }
}
