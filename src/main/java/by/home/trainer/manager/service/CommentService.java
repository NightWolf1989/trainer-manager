package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.comment.CommentCreateDto;
import by.home.trainer.manager.dto.comment.CommentFullDto;
import by.home.trainer.manager.dto.comment.CommentPreviewDto;
import by.home.trainer.manager.dto.comment.CommentUpdateDto;
import by.home.trainer.manager.entity.Comment;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto);

    CommentFullDto update(CommentUpdateDto updateDto);

    void deleteById(Long id);
}
