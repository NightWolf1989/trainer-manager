package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.admin.AdminCreateDto;
import by.home.trainer.manager.dto.admin.AdminFullDto;
import by.home.trainer.manager.dto.admin.AdminPreviewDto;
import by.home.trainer.manager.dto.admin.AdminUpdateDto;
import by.home.trainer.manager.entity.Admin;

import java.util.List;

public interface AdminService {

    List<AdminPreviewDto> findAll();

    AdminFullDto findById(Long id);

    AdminFullDto create(AdminCreateDto createDto);

    AdminFullDto update(AdminUpdateDto updateDto);

    void deleteById(Long id);

}
