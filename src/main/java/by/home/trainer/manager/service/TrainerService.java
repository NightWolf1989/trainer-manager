package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.trainer.TrainerCreateDto;
import by.home.trainer.manager.dto.trainer.TrainerFullDto;
import by.home.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.home.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.home.trainer.manager.entity.Trainer;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById(Long id);

    TrainerFullDto create(TrainerCreateDto createDto);

    TrainerFullDto update(TrainerUpdateDto updateDto);

    void deleteById(Long id);
}
