package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.recording.RecordingCreateDto;
import by.home.trainer.manager.dto.recording.RecordingFullDto;
import by.home.trainer.manager.dto.recording.RecordingPreviewDto;
import by.home.trainer.manager.dto.recording.RecordingUpdateDto;
import by.home.trainer.manager.entity.Recording;

import java.util.List;

public interface RecordingService {

    List<RecordingPreviewDto> findAll();

    RecordingFullDto findById(Long id);

    RecordingFullDto create(RecordingCreateDto createDto);

    RecordingFullDto update(RecordingUpdateDto updateDto);

    void deleteById(Long id);
}
