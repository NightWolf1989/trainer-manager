package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.comment.CommentCreateDto;
import by.home.trainer.manager.dto.comment.CommentFullDto;
import by.home.trainer.manager.dto.comment.CommentPreviewDto;
import by.home.trainer.manager.dto.comment.CommentUpdateDto;
import by.home.trainer.manager.entity.Comment;
import by.home.trainer.manager.entity.Trainer;
import by.home.trainer.manager.mapper.CommentMapper;
import by.home.trainer.manager.repository.CommentRepository;
import by.home.trainer.manager.repository.CommentRepositoryImpl;
import by.home.trainer.manager.repository.TrainerRepository;
import by.home.trainer.manager.repository.TrainerRepositoryImpl;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private CommentMapper mapper = new CommentMapper();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(Long id) {
        Comment foundById = commentRepository.findById(id);

        return mapper.mapToDto(foundById);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Comment toSave = mapper.mapToEntity(createDto, trainer);

        Comment create = commentRepository.create(toSave);

        CommentFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {
        Comment entityToUpdate = mapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());

        entityToUpdate.setUserName(existingEntity.getUserName());
        entityToUpdate.setUserSurname(existingEntity.getUserSurname());
        entityToUpdate.setUserEmail(existingEntity.getUserEmail());
        entityToUpdate.setRatingToTheCoach(existingEntity.getRatingToTheCoach());
        entityToUpdate.setPosted(existingEntity.getPosted());
        entityToUpdate.setTrainer(existingEntity.getTrainer());

        Comment create = commentRepository.update(entityToUpdate);

        CommentFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);

    }
}
