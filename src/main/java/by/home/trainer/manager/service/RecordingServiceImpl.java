package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.recording.RecordingCreateDto;
import by.home.trainer.manager.dto.recording.RecordingFullDto;
import by.home.trainer.manager.dto.recording.RecordingPreviewDto;
import by.home.trainer.manager.dto.recording.RecordingUpdateDto;
import by.home.trainer.manager.entity.Recording;
import by.home.trainer.manager.entity.Trainer;
import by.home.trainer.manager.mapper.RecordingMapper;
import by.home.trainer.manager.repository.RecordingRepository;
import by.home.trainer.manager.repository.RecordingRepositoryImpl;
import by.home.trainer.manager.repository.TrainerRepository;
import by.home.trainer.manager.repository.TrainerRepositoryImpl;

import java.util.List;

public class RecordingServiceImpl implements RecordingService {

    private RecordingRepository recordingRepository = new RecordingRepositoryImpl();
    private RecordingMapper mapper = new  RecordingMapper();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @Override
    public List<RecordingPreviewDto> findAll() {
        List<Recording> found = recordingRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public RecordingFullDto findById(Long id) {
        Recording foundById = recordingRepository.findById(id);

        return mapper.mapToDto(foundById);
    }

    @Override
    public RecordingFullDto create(RecordingCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Recording toSave = mapper.mapToEntity(createDto, trainer);

        Recording create = recordingRepository.create(toSave);

        RecordingFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public RecordingFullDto update(RecordingUpdateDto updateDto) {
        Trainer trainer = trainerRepository.findById(updateDto.getTrainerId());
        Recording entityToUpdate = mapper.mapToEntity(updateDto, trainer);
        Recording existingEntity = recordingRepository.findById(updateDto.getId());

        entityToUpdate.setUserName(existingEntity.getUserName());
        entityToUpdate.setUserEmail(existingEntity.getUserEmail());
        entityToUpdate.setSurname(existingEntity.getSurname());
        entityToUpdate.setPhoneNumber(existingEntity.getPhoneNumber());

        Recording create = recordingRepository.update(entityToUpdate);

        RecordingFullDto response = mapper.mapToDto(create);
        return response;
    }

    @Override
    public void deleteById(Long id) {
        recordingRepository.deleteById(id);

    }
}
