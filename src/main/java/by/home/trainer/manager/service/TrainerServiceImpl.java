package by.home.trainer.manager.service;

import by.home.trainer.manager.dto.trainer.TrainerCreateDto;
import by.home.trainer.manager.dto.trainer.TrainerFullDto;
import by.home.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.home.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.home.trainer.manager.entity.Trainer;
import by.home.trainer.manager.mapper.TrainerMapper;
import by.home.trainer.manager.repository.TrainerRepository;
import by.home.trainer.manager.repository.TrainerRepositoryImpl;

import java.util.List;

public class TrainerServiceImpl implements TrainerService {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private TrainerMapper mapper = new TrainerMapper();

    @Override
    public List<TrainerPreviewDto> findAll() {
        List<Trainer> found = trainerRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public TrainerFullDto findById(Long id) {
        Trainer foundById = trainerRepository.findById(id);

        return mapper.mapToDto(foundById);
    }

    @Override
    public TrainerFullDto create(TrainerCreateDto createDto) {
        Trainer toSave = mapper.mapToEntity(createDto);
        if (toSave.getImageAvatar() == null){
            toSave.setImageAvatar("https://klike.net/uploads/posts/2019-03/1551511801_1.jpg");
        }
        Trainer create = trainerRepository.create(toSave);

        return mapper.mapToDto(create);
    }

    @Override
    public TrainerFullDto update(TrainerUpdateDto updateDto) {
        Trainer entityToUpdate = mapper.mapToEntity(updateDto);
        Trainer existingEntity = trainerRepository.findById(updateDto.getId());

        entityToUpdate.setWorkExperience(existingEntity.getWorkExperience());
        entityToUpdate.setEmail(existingEntity.getEmail());
        entityToUpdate.setPassword(existingEntity.getPassword());
        entityToUpdate.setComments(existingEntity.getComments());
        entityToUpdate.setRecordings(existingEntity.getRecordings());

        Trainer create = trainerRepository.update(entityToUpdate);

        return mapper.mapToDto(create);
    }

    @Override
    public void deleteById(Long id) {
        trainerRepository.deleteById(id);
    }
}
