package by.home.trainer.manager.mapper;

import by.home.trainer.manager.dto.admin.AdminCreateDto;
import by.home.trainer.manager.dto.admin.AdminFullDto;
import by.home.trainer.manager.dto.admin.AdminPreviewDto;
import by.home.trainer.manager.dto.admin.AdminUpdateDto;
import by.home.trainer.manager.entity.Admin;

import java.util.ArrayList;
import java.util.List;

public class AdminMapper {

    public List<AdminPreviewDto> mapToDtoList(List<Admin> entities){
        List<AdminPreviewDto> dtos = new ArrayList<>();

        for (Admin entity : entities){

            AdminPreviewDto dto = new AdminPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setRole(entity.getRole());

            dtos.add(dto);
        }

        return dtos;
    }

    public Admin mapToEntity(AdminCreateDto createDto){
        Admin admin = new Admin();
        admin.setEmail(createDto.getEmail());
        admin.setPassword(createDto.getPassword());
        admin.setName(createDto.getName());
        admin.setSurname(createDto.getSurname());
        // admin.setRole(Role.ADMIN); ??

        return admin;
    }

    public Admin mapToEntity(AdminUpdateDto updateDto){
        Admin admin = new Admin();
        admin.setId(updateDto.getId());
        admin.setName(updateDto.getName());
        admin.setSurname(updateDto.getSurname());

        return admin;
    }

    public AdminFullDto mapToDto (Admin entity){
        AdminFullDto fullDto  = AdminFullDto.builder()
                .id(entity.getId())
                .email(entity.getEmail())
                .name(entity.getName())
                .password(entity.getPassword())
                .role(entity.getRole())
                .surname(entity.getSurname())
                .build();

        return fullDto;
    }
}
