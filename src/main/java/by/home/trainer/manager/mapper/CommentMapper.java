package by.home.trainer.manager.mapper;

import by.home.trainer.manager.dto.comment.CommentCreateDto;
import by.home.trainer.manager.dto.comment.CommentFullDto;
import by.home.trainer.manager.dto.comment.CommentPreviewDto;
import by.home.trainer.manager.dto.comment.CommentUpdateDto;
import by.home.trainer.manager.entity.Comment;
import by.home.trainer.manager.entity.Trainer;
import org.apache.catalina.User;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities){
        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (Comment entity : entities){

            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(entity.getId());
            dto.setMessage(entity.getMessage());
            dto.setRatingToTheCoach(entity.getRatingToTheCoach());
            dto.setUserName(entity.getUserName());
            dto.setUserSurname(entity.getUserSurname());
            dto.setTrainerName(entity.getTrainer().getName());

            dtos.add(dto);
        }

        return dtos;
    }

    public Comment mapToEntity(CommentCreateDto createDto, Trainer trainer){
        Comment comment = new Comment();

        comment.setMessage(createDto.getMessage());
        comment.setTrainer(trainer);
        comment.setUserEmail(createDto.getUserEmail());
        comment.setUserName(createDto.getUserName());
        comment.setUserSurname(createDto.getUserSurname());
//        comment.setRatingToTheCoach(createDto.getRatingToTheCoach());

        return comment;

    }

    public Comment mapToEntity(CommentUpdateDto updateDto){
        Comment comment = new Comment();
        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());

        return comment;
    }

    public CommentFullDto mapToDto(Comment entity){
        CommentFullDto fullDto = CommentFullDto.builder()
                .id(entity.getId())
                .message(entity.getMessage())
                .posted(entity.getPosted())
                .ratingToTheCoach(entity.getRatingToTheCoach())
                .trainer(entity.getTrainer())
                .userEmail(entity.getUserEmail())
                .userName(entity.getUserName())
                .userSurname(entity.getUserSurname())
                .build();

        return fullDto;
    }
}
