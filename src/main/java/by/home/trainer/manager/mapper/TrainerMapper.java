package by.home.trainer.manager.mapper;

import by.home.trainer.manager.dto.trainer.TrainerCreateDto;
import by.home.trainer.manager.dto.trainer.TrainerFullDto;
import by.home.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.home.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.home.trainer.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList(List<Trainer> entities) {
        List<TrainerPreviewDto> dtos = new ArrayList<>();

        for (Trainer entity : entities){
            TrainerPreviewDto dto = new TrainerPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setEmail(entity.getEmail());
            dto.setImageAvatar(entity.getImageAvatar());
            dto.setSurname(entity.getSurname());
            dto.setWorkExperience(entity.getWorkExperience());

            dtos.add(dto);
        }

        return dtos;
    }

    public Trainer mapToEntity(TrainerCreateDto createDto){
        Trainer trainer = new Trainer();
        trainer.setName(createDto.getName());
        trainer.setSurname(createDto.getSurname());
        trainer.setImageAvatar(createDto.getImageAvatar());
        trainer.setEmail(createDto.getEmail());
        trainer.setPassword(createDto.getPassword());
        trainer.setWorkExperience(createDto.getWorkExperience());

        return trainer;
    }

    public Trainer mapToEntity(TrainerUpdateDto updateDto){
        Trainer trainer = new Trainer();
        trainer.setId(updateDto.getId());
        trainer.setSurname(updateDto.getSurname());
        trainer.setName(updateDto.getName());
        trainer.setImageAvatar(updateDto.getImageAvatar());

        return trainer;

    }

    public TrainerFullDto mapToDto(Trainer entity){
        TrainerFullDto fullDto = TrainerFullDto.builder()
                .email(entity.getEmail())
                .name(entity.getName())
                .comments(entity.getComments())
                .imageAvatar(entity.getImageAvatar())
                .surname(entity.getSurname())
                .recordings(entity.getRecordings())
                .workExperience(entity.getWorkExperience())
                .id(entity.getId())
                .password(entity.getPassword())
                .build();
        //..
        return fullDto;
    }
}
