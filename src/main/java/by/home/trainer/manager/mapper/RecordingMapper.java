package by.home.trainer.manager.mapper;

import by.home.trainer.manager.dto.recording.RecordingCreateDto;
import by.home.trainer.manager.dto.recording.RecordingFullDto;
import by.home.trainer.manager.dto.recording.RecordingPreviewDto;
import by.home.trainer.manager.dto.recording.RecordingUpdateDto;
import by.home.trainer.manager.entity.Recording;
import by.home.trainer.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class RecordingMapper {

    public List<RecordingPreviewDto> mapToDtoList(List<Recording> entities){
        List<RecordingPreviewDto> dtos = new ArrayList<>();

        for (Recording entity : entities){
            RecordingPreviewDto dto = new RecordingPreviewDto();

            dto.setId(entity.getId());
            dto.setDay(entity.getDay());
            dto.setMessage(entity.getMessage());
            dto.setSurname(entity.getSurname());
            dto.setUserName(entity.getUserName());
            dto.setTime(entity.getTime());
            dto.setTrainerName(entity.getTrainer().getName() + " " + entity.getTrainer().getSurname());

            dtos.add(dto);
        }

        return dtos;

    }

    public Recording mapToEntity(RecordingCreateDto createDto, Trainer trainer){
        Recording recording = new Recording();
        recording.setDay(createDto.getDay());
        recording.setTime(createDto.getTime());
        recording.setMessage(createDto.getMessage());
        recording.setPhoneNumber(createDto.getPhoneNumber());
        recording.setSurname(createDto.getSurname());
        recording.setUserName(createDto.getUserName());
        recording.setUserEmail(createDto.getUserEmail());
        recording.setTrainer(trainer);

        return recording;
    }

    public Recording mapToEntity(RecordingUpdateDto updateDto, Trainer trainer){
        Recording recording = new Recording();
        recording.setId(updateDto.getId());
        recording.setTrainer(trainer);
        recording.setTime(updateDto.getTime());
        recording.setDay(updateDto.getDay());
        recording.setMessage(updateDto.getMessage());

        return recording;
    }

    public RecordingFullDto mapToDto(Recording entity) {
        RecordingFullDto fullDto = RecordingFullDto.builder()
                .id(entity.getId())
                .userName(entity.getUserName())
                .surname(entity.getSurname())
                .trainer(entity.getTrainer())
                .time(entity.getTime())
                .day(entity.getDay())
                .userEmail(entity.getUserEmail())
                .message(entity.getMessage())
                .phoneNumber(entity.getPhoneNumber())
                .build();

        return fullDto;
    }
}
