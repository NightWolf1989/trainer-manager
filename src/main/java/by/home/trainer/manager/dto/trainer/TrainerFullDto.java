package by.home.trainer.manager.dto.trainer;

import by.home.trainer.manager.entity.Comment;
import by.home.trainer.manager.entity.Recording;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainerFullDto {

    private Long id;
    private String name;
    private String surname;
    private String workExperience;
    private String imageAvatar;
    private String email;
    private String password;
    private List<Comment> comments;
    private List<Recording> recordings;

}
