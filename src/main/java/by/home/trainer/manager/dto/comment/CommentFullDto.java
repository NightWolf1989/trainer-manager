package by.home.trainer.manager.dto.comment;

import by.home.trainer.manager.entity.Trainer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentFullDto {

    private Long id;
    private String message;
    private String userName;
    private String userSurname;
    private String userEmail;
    private Long ratingToTheCoach;
    private Boolean posted;
    private Trainer trainer;
}
