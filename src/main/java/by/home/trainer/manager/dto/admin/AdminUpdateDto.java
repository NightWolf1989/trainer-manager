package by.home.trainer.manager.dto.admin;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminUpdateDto {

    private Long id;
    private String name;
    private String surname;

}
