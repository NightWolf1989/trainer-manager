package by.home.trainer.manager.dto.recording;

import by.home.trainer.manager.entity.Trainer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RecordingFullDto {

    private Long id;
    private String time;
    private String day;
    private String message;
    private String userName;
    private String surname;
    private String userEmail;
    private String phoneNumber;
    private Trainer trainer;
}
