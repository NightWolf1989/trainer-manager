package by.home.trainer.manager.dto.recording;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RecordingCreateDto {

    private String time;
    private String day;
    private String message;
    private String userName;
    private String surname;
    private String userEmail;
    private String phoneNumber;
    private Long trainerId;

}
