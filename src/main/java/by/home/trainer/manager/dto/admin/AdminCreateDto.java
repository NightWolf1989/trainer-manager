package by.home.trainer.manager.dto.admin;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminCreateDto {

    private String name;
    private String surname;
    private String email;
    private String password;

}
