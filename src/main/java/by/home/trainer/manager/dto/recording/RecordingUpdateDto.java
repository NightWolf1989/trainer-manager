package by.home.trainer.manager.dto.recording;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RecordingUpdateDto {


    private Long id;
    private String time;
    private String day;
    private String message;
    private Long trainerId;

}
