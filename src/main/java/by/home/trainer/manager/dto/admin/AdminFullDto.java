package by.home.trainer.manager.dto.admin;

import by.home.trainer.manager.entity.enums.Role;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminFullDto {

    private Long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private Role role;

}
