package by.home.trainer.manager.dto.trainer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainerCreateDto {

    private String name;
    private String surname;
    private String workExperience;
    private String imageAvatar;
    private String email;
    private String password;

}
