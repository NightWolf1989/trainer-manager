package by.home.trainer.manager.dto.admin;

import by.home.trainer.manager.entity.enums.Role;
import lombok.Data;

@Data
public class AdminPreviewDto {

    private Long id;
    private String name;
    private String surname;
    private Role role;

}
