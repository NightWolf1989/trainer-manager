package by.home.trainer.manager.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentCreateDto {

    private String message;
    private String userName;
    private String userSurname;
    private String userEmail;
//    private Boolean posted;
    private Long trainerId;
//    private Long ratingToTheCoach;
}
