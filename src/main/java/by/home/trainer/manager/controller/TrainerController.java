package by.home.trainer.manager.controller;

import by.home.trainer.manager.dto.comment.CommentCreateDto;
import by.home.trainer.manager.dto.comment.CommentFullDto;
import by.home.trainer.manager.dto.recording.RecordingCreateDto;
import by.home.trainer.manager.dto.recording.RecordingFullDto;
import by.home.trainer.manager.dto.trainer.TrainerCreateDto;
import by.home.trainer.manager.dto.trainer.TrainerFullDto;
import by.home.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.home.trainer.manager.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class TrainerController {

    private TrainerService trainerService = new TrainerServiceImpl();
    private CommentService commentService = new CommentServiceImpl();
    private RecordingService recordingService = new RecordingServiceImpl();

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model){

        List<TrainerPreviewDto> found = trainerService.findAll();

        model.addAttribute("all_trainer", found);
        return "index";

    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer-profile/{id}")
    public String openProfileTrainer(@PathVariable Long id, Model model){
        TrainerFullDto fullDto = trainerService.findById(id);
        model.addAttribute("found_by_id_trainer", fullDto);
        model.addAttribute("createdCommentDto", new CommentCreateDto());

        return "trainer-profile";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comment/create")
    public String saveComment(@ModelAttribute CommentCreateDto createDto){
        System.out.println("Сообщение от пользователя: " + createDto);
        CommentFullDto created = commentService.create(createDto);
        System.out.println(created);

        return "redirect:/trainer-profile/" + createDto.getTrainerId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openRegistration(Model model){
        model.addAttribute("createDto",new TrainerCreateDto());

        return "sing_in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainer/create")
    public String saveTrainer(@ModelAttribute CommentCreateDto createDto){
        System.out.println("Что получили от пользователя: " +createDto);
        CommentFullDto created = commentService.create(createDto);

        return "redirect:/trainer-profile/" + created.getTrainer().getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/recording/{id}")
    public String openRecordingTrainer(@PathVariable Long id, Model model){
        TrainerFullDto fullDto = trainerService.findById(id);
        model.addAttribute("found_by_id_trainer", fullDto);
        model.addAttribute("createRecording", new RecordingCreateDto());
        return "recording";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/recording/create" )
    public String saveRecording (@ModelAttribute RecordingCreateDto createDto){
        System.out.println("Запись: " + createDto);
         recordingService.create(createDto);

        return "redirect:/index";
    }

}
