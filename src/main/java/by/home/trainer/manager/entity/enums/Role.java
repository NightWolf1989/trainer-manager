package by.home.trainer.manager.entity.enums;

public enum Role {

    ADMIN, SUPERUSER
}
