package by.home.trainer.manager.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "recording")
public class Recording {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "time", nullable = false)
    private String time;

    @Column(name = "day", nullable = false)
    private String day;

    @Column(name = "message")
    private String message;

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "user_email", nullable = false, unique = true)
    private String userEmail;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private Trainer trainer;


}
